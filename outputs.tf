######
## Hosted zone
######

output "delegation_set_ids" {
  value = length(local.delegation_sets) != 0 ? { for k, v in aws_route53_delegation_set.this : k => v.id } : null
}

output "delegation_set_arns" {
  value = length(local.delegation_sets) != 0 ? { for k, v in aws_route53_delegation_set.this : k => v.arn } : null
}

output "delegation_set_name_servers" {
  value = length(local.delegation_sets) != 0 ? { for k, v in aws_route53_delegation_set.this : k => v.name_servers } : null
}

output "public_zone_arns" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.arn } : null
}

output "public_zone_ids" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.zone_id } : null
}

output "public_zone_domain_names" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.name } : null
}

output "public_zone_name_servers" {
  value = length(local.public_zones) != 0 ? { for k, v in aws_route53_zone.public : k => v.name_servers } : null
}

output "private_zone_arns" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.arn } : null
}

output "private_zone_ids" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.zone_id } : null
}

output "private_zone_domain_names" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.name } : null
}

output "private_zone_name_servers" {
  value = length(local.private_zones) != 0 ? { for k, v in aws_route53_zone.private : k => v.name_servers } : null
}

#####
# DNSSEC
#####

output "dnssec_ksk_ids" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.id } : null
}

output "dnssec_ksk_records" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.dnskey_record } : null
}

output "dnssec_ksk_ds_records" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.ds_record } : null
}

output "dnssec_ksk_public_keys" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.public_key } : null
}

output "dnssec_ksk_digest_values" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.digest_value } : null
}

output "dnssec_ksk_digest_algorithm_types" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.digest_algorithm_type } : null
}

output "dnssec_ksk_digest_algorithm_mnemonics" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.digest_algorithm_mnemonic } : null
}

output "dnssec_ksk_signing_algorithm_type" {
  value = length(local.dnssec_zones) != 0 ? { for k, v in aws_route53_key_signing_key.this : k => v.signing_algorithm_type } : null
}

output "dnssec_kms_key_id" {
  value = element(concat(aws_kms_key.this_dnssec.*.id, [null]), 0)
}

output "dnssec_kms_key_arn" {
  value = element(concat(aws_kms_key.this_dnssec.*.arn, [null]), 0)
}

output "dnssec_kms_key_alias_arn" {
  value = element(concat(aws_kms_alias.this_dnssec.*.arn, [null]), 0)
}

######
## Resolver endpoints inbound
######

output "resolver_inbound_security_group_ids" {
  value = length(aws_security_group.this_inbound) > 0 ? aws_security_group.this_inbound.*.id : null
}

output "resolver_inbound_ids" {
  value = length(aws_route53_resolver_endpoint.this_inbound) > 0 ? aws_route53_resolver_endpoint.this_inbound.*.id : null
}

output "resolver_inbound_arns" {
  value = length(aws_route53_resolver_endpoint.this_inbound) > 0 ? aws_route53_resolver_endpoint.this_inbound.*.arn : null
}

output "resolver_inbound_host_vpc_ids" {
  value = length(aws_route53_resolver_endpoint.this_inbound) > 0 ? aws_route53_resolver_endpoint.this_inbound.*.host_vpc_id : null
}

#####
# Resolver endpoints outbound
#####

output "resolver_outbound_security_group_id" {
  value = length(aws_security_group.this_outbound) > 0 ? aws_security_group.this_outbound.*.id : null
}

output "resolver_outbound_ids" {
  value = length(aws_route53_resolver_endpoint.this_outbound) > 0 ? aws_route53_resolver_endpoint.this_outbound.*.id : null
}

output "resolver_outbound_arns" {
  value = length(aws_route53_resolver_endpoint.this_outbound) > 0 ? aws_route53_resolver_endpoint.this_outbound.*.arn : null
}

output "resolver_outbound_host_vpc_ids" {
  value = length(aws_route53_resolver_endpoint.this_outbound) > 0 ? aws_route53_resolver_endpoint.this_outbound.*.host_vpc_id : null
}

#####
# Forward rules
#####

output "rule_forward_ids" {
  value = length(aws_route53_resolver_rule.this_forward) > 0 ? aws_route53_resolver_rule.this_forward.*.id : null
}

output "rule_forward_arns" {
  value = length(aws_route53_resolver_rule.this_forward) > 0 ? aws_route53_resolver_rule.this_forward.*.arn : null
}

output "rule_forward_owner_ids" {
  value = length(aws_route53_resolver_rule.this_forward) > 0 ? aws_route53_resolver_rule.this_forward.*.owner_id : null
}

output "rule_forward_share_statuses" {
  value = length(aws_route53_resolver_rule.this_forward) > 0 ? aws_route53_resolver_rule.this_forward.*.share_status : null
}

output "rule_association_forward_id" {
  value = length(aws_route53_resolver_rule_association.this_forward) > 0 ? aws_route53_resolver_rule_association.this_forward.*.id : null
}

#####
# Resource share
#####

output "rule_forward_share_ids" {
  value = length(aws_ram_resource_share.this_forward) > 0 ? aws_ram_resource_share.this_forward.*.id : null
}

output "rule_forward_share_arns" {
  value = length(aws_ram_resource_share.this_forward) > 0 ? aws_ram_resource_share.this_forward.*.arn : null
}

output "resource_association_forward_id" {
  value = length(aws_ram_resource_association.this_forward) > 0 ? aws_ram_resource_association.this_forward.*.id : null
}

output "principal_association_forward_id" {
  value = length(aws_ram_principal_association.this_forward) > 0 ? aws_ram_principal_association.this_forward.*.id : null
}

#####
# Records
#####

output "record_names" {
  value = length(local.records) != 0 || length(local.alias_records) != 0 ? merge(
    { for k, v in aws_route53_record.this : k => v.name },
    { for k, v in aws_route53_record.this_alias : k => v.name }
  ) : null
}

output "record_fqdns" {
  value = length(local.records) != 0 || length(local.alias_records) != 0 ? merge(
    { for k, v in aws_route53_record.this : k => v.fqdn },
    { for k, v in aws_route53_record.this_alias : k => v.fqdn }
  ) : null
}
